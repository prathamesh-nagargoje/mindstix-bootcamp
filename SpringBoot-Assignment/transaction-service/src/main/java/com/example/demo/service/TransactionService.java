package com.example.demo.service;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.model.Transaction;
import com.example.demo.model.TransactionDTO;
import com.example.demo.repo.CustomerRepository;
import com.example.demo.repo.TransactionRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@Service
public class TransactionService {

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private TransactionRepository transactionRepository;

  @Autowired
  private RabbitTemplate rabbitTemplate;

  @Autowired
  private FanoutExchange fanout;

  public TransactionDTO getTransactionDTO(Transaction transaction) {
    return TransactionDTO.builder().accountNo(transaction.getAccountNo())
        .type(transaction.getType()).ammount(transaction.getAmmount()).build();
  }

  @CircuitBreaker(name = "transactionBreaker", fallbackMethod = "fallback")
  public void saveTransaction(Transaction transaction) {
    try {
      transaction = transactionRepository.save(transaction);
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public void fallback(Transaction transaction, Exception e) {
    System.out.println(e);
  }

  public int getAccountNumber(String email) {
    int accNo = customerRepository.getAccountNumber(email);
    return accNo;
  }

  public void sendMessage(TransactionDTO transactionDTO) {
    rabbitTemplate.convertAndSend(fanout.getName(), "", transactionDTO);
  }
}
