package com.example.demo.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.example.demo.model.Transaction;

public interface TransactionRepository extends MongoRepository<Transaction, Integer> {

}
