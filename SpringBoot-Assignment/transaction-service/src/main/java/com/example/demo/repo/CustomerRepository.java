package com.example.demo.repo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "customer-service")
public interface CustomerRepository {

  @Retryable(value = RuntimeException.class, maxAttempts = 2)
  @GetMapping("/customer/account")
  int getAccountNumber(@RequestParam("email") String email);
}
