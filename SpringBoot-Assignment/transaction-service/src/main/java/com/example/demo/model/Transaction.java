package com.example.demo.model;

import javax.persistence.Entity;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Transaction {

  private String email;
  private String type;
  private double ammount;
  private int accountNo;
}
