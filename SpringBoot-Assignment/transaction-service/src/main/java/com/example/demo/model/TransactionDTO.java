package com.example.demo.model;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude
public class TransactionDTO implements Serializable {

  private String type;
  private double ammount;
  private int accountNo;

}
