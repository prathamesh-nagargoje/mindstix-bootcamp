package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Transaction;
import com.example.demo.model.TransactionDTO;
import com.example.demo.service.TransactionService;

@RestController
@RequestMapping("/transaction")
public class TransactionController {

  @Autowired
  private TransactionService transactionService;

  @PostMapping
  public ResponseEntity<String> doTransaction(@RequestBody Transaction transaction) {
    try {
      int accNo = transactionService.getAccountNumber(transaction.getEmail());
      transaction.setAccountNo(accNo);
      transactionService.saveTransaction(transaction);
      TransactionDTO transactionDTO = transactionService.getTransactionDTO(transaction);
      transactionService.sendMessage(transactionDTO);
      return ResponseEntity.accepted().body("Transaction Successful.");
    } catch (Exception e) {
      System.out.println(e);
      return ResponseEntity.badRequest().body("Transaction Failed.");
    }
  }
}
