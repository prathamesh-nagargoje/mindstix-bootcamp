package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Customer;
import com.example.demo.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

  @Autowired
  private CustomerService customerService;

  @PostMapping
  public ResponseEntity<Object> createCustomer(@RequestParam("email") String email) {
    try {
      Customer customer = customerService.createCustomer(email);
      customer = customerService.saveCustomer(customer);
      return ResponseEntity.status(201).body(customer);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body("Something went wrong");
    }
  }

  @GetMapping
  public ResponseEntity<Object> getCustomer(@RequestParam("email") String email) {
    try {
      Customer customer = customerService.getCustomerByEmail(email);
      return ResponseEntity.ok().body(customer);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body("Customer not found");
    }
  }

  @GetMapping("/account")
  public int getAccountNumber(@RequestParam("email") String email) {
    Customer customer = customerService.getCustomerByEmail(email);
    return customer.getAccount().getId();
  }
}
