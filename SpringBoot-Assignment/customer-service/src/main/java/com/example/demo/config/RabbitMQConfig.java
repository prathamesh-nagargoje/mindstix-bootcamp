package com.example.demo.config;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import com.example.demo.model.Customer;
import com.example.demo.model.TransactionDTO;
import com.example.demo.service.CustomerService;

@Configuration
public class RabbitMQConfig {

  @Autowired
  private CustomerService customerService;

  @RabbitListener(queues = "tQueue")
  public void receive(TransactionDTO in) {
    try {
      TransactionDTO transaction = TransactionDTO.builder().accountNo(in.getAccountNo())
          .ammount(in.getAmmount()).type(in.getType()).build();
      Customer customer = customerService.getCustomerByAccNo(transaction.getAccountNo());
      if (customer == null)
        return;
      customer = customerService.makeTransaction(customer, transaction.getType(),
          transaction.getAmmount());
      customer = customerService.saveCustomer(customer);
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
