package com.example.demo.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.model.Account;
import com.example.demo.model.Customer;
import com.example.demo.model.repo.CustomerRepo;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;

@Service
public class CustomerService {

  @Autowired
  private CustomerRepo customerRepo;

  public Customer createCustomer(String email) {
    Customer customer = new Customer();
    customer.setEmail(email);
    Account account = new Account();
    account.setBalance(0);
    customer.setAccount(account);
    return customer;
  }

  @CircuitBreaker(name = "backend", fallbackMethod = "fallback")
  public Customer saveCustomer(Customer customer) {
    customer = customerRepo.save(customer);
    return customer;
  }

  @CircuitBreaker(name = "backend", fallbackMethod = "fallback")
  @Retry(name = "backend", fallbackMethod = "fallback")
  public Customer getCustomerByEmail(String email) {
    Optional<Customer> customer = customerRepo.findByEmail(email);
    if (customer.isPresent())
      return customer.get();
    return null;
  }

  @CircuitBreaker(name = "circuitBreaker", fallbackMethod = "fallback")
  @Retry(name = "backend", fallbackMethod = "fallback")
  public Customer getCustomer(int id) {
    Optional<Customer> customer = customerRepo.findById(id);
    if (customer.isPresent())
      return customer.get();
    return null;
  }

  private Customer fallback(Customer customer, Exception e) {
    System.out.println(e);
    return null;
  }

  private Customer fallback(int id, Exception e) {
    System.out.println(e);
    return null;
  }

  private Customer fallback(String email, Exception e) {
    System.out.println(e);
    return null;
  }

  public Customer getCustomerByAccNo(int accNo) {
    try {
      Optional<Customer> optional = Optional.ofNullable(customerRepo.findByAccNo(accNo));
      if (optional.isPresent())
        return optional.get();
      return null;
    } catch (Exception e) {
      System.out.println("Error: " + e);
      return null;
    }
  }

  public Customer makeTransaction(Customer customer, String type, double ammount) {
    try {
      Account account = customer.getAccount();
      double newBalance = 0.0;
      if (type.equalsIgnoreCase("credit")) {
        newBalance = account.getBalance() + ammount;
      } else if (type.equalsIgnoreCase("debit")) {
        newBalance = account.getBalance() - ammount;
      }
      account.setBalance(newBalance);
      customer.setAccount(account);
      return customer;
    } catch (Exception e) {
      System.out.println(e);
      return null;
    }
  }

}
