package com.example.demo.model.repo;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.demo.model.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {

  Optional<Customer> findByEmail(String email);

  @Query("select c from Customer c where c.account.id = ?1")
  Customer findByAccNo(int accNO);
}
