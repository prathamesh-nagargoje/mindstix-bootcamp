package com.example.demo.model;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransactionDTO implements Serializable {

  private String type;
  private double ammount;
  private int accountNo;

}
