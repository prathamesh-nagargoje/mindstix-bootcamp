package com.example.demo.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.example.demo.model.Customer;

public interface CustomerRepo extends CrudRepository<Customer, Integer> {

  List<Customer> findByName(String name);
}
