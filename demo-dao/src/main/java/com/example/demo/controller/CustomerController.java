package com.example.demo.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.dao.CustomerDao;
import com.example.demo.model.Customer;

@RestController
@RequestMapping("/customer")
public class CustomerController {

  @Autowired
  private CustomerDao<Customer> customerDao;

  @GetMapping
  public ResponseEntity<Object> getAll() {
    return ResponseEntity.ok(customerDao.findAll());
  }

  @GetMapping("/{id}")
  public ResponseEntity<Object> getById(@PathVariable("id") int id) {
    Optional<Customer> customer = customerDao.findById(id);
    if (customer.isPresent())
      return ResponseEntity.ok(customer.get());
    return ResponseEntity.badRequest().body("Customer not found");
  }

  @PostMapping
  public ResponseEntity<Object> createCustomer(@RequestBody Customer customer) {
    if (customerDao.save(customer))
      return ResponseEntity.ok("Customer is created");
    return ResponseEntity.badRequest().body("Customer already present");
  }
}
