package com.example.demo.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Repository;
import com.example.demo.model.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDao<Customer> {

  private Map<Integer, Customer> repository = new HashMap<>();

  @Override
  public Optional<Customer> findById(int id) {
    Optional<Customer> customer = null;
    if (repository.containsKey(id))
      customer = Optional.ofNullable(repository.get(id));
    return customer;
  }

  @Override
  public List<Customer> findAll() {
    return repository.values().stream().collect(Collectors.toList());
  }

  @Override
  public boolean save(Customer customer) {
    if (repository.containsKey(customer.getId()))
      return false;
    repository.put(customer.getId(), customer);
    return true;
  }


}
