package com.example.demo.dao;

import java.util.List;
import java.util.Optional;

public interface CustomerDao<T> {

  Optional<T> findById(int id);

  List<T> findAll();

  boolean save(T t);
}
