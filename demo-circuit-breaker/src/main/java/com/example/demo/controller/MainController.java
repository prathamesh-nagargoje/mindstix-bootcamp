package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class MainController {

  @Autowired
  private CustomerService customerService;

  @GetMapping
  public ResponseEntity<Object> getCustomer() {
    return ResponseEntity.ok(customerService.getCustomer());
  }
}
