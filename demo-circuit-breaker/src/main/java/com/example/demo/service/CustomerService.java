package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CustomerService {

  @Autowired
  private RestTemplate restTemplate;
  @Autowired
  private CircuitBreakerFactory circuitBreakerFactory;

  public String getCustomer() {
    CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitBreaker");
    String url = "http://localhost:8088/customer";
    return circuitBreaker.run(() -> restTemplate.getForObject(url, String.class),
        throwable -> getDefaultCustomer());
  }

  private String getDefaultCustomer() {
    return "circuit breaker executed";
  }
}
