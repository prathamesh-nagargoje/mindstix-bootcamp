package com.example.demo.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Customer;
import com.example.demo.repo.CustomerRepo;

@RestController
@RequestMapping("/customer")
public class CustomerController {

  @Autowired
  private CustomerRepo customerRepo;

  @GetMapping
  public ResponseEntity<Object> getAll() {
    return ResponseEntity.ok(customerRepo.findAll());
  }

  @GetMapping("/{id}")
  public ResponseEntity<Object> getById(@PathVariable("id") int id) {
    Optional<Customer> customer = customerRepo.findById(id);
    if (customer.isPresent())
      return ResponseEntity.ok(customer.get());
    return ResponseEntity.badRequest().body("Customer not found");
  }

  @GetMapping("/name/{name}")
  public ResponseEntity<Object> getByName(@PathVariable("name") String name) {
    List<Customer> customer = customerRepo.findByName(name);
    return ResponseEntity.ok(customer);
  }

  @PostMapping
  public ResponseEntity<Object> createCustomer(@RequestBody Customer customer) {
    customerRepo.save(customer);
    return ResponseEntity.ok("Customer is created");
  }
}
