package com.example.demo.controller;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rmq")
public class MainController {

  @Autowired
  private RabbitTemplate rabbitTemplate;

  @Autowired
  private FanoutExchange fanout;

  @GetMapping
  public ResponseEntity<String> sendMessage(@RequestParam("message") String message) {
    rabbitTemplate.convertAndSend(fanout.getName(), "", message);
    return ResponseEntity.accepted().body("Message send successful to " + fanout.getName());
  }
}
