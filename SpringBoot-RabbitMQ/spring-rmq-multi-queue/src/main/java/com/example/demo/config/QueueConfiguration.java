package com.example.demo.config;

import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfiguration {

  @Bean
  public FanoutExchange fanout() {
    return new FanoutExchange("fanout");
  }

  @Bean
  public Queue anonymousQueue1() {
    return new AnonymousQueue();
  }

  @Bean
  public Queue anonymousQueue2() {
    return new AnonymousQueue();
  }

  @Bean
  public Binding binding1(FanoutExchange fanout, Queue anonymousQueue1) {
    return BindingBuilder.bind(anonymousQueue1).to(fanout);
  }

  @Bean
  public Binding binding2(FanoutExchange fanout, Queue anonymousQueue2) {
    return BindingBuilder.bind(anonymousQueue2).to(fanout);
  }

  @RabbitListener(queues = "#{anonymousQueue1.name}")
  public void receiver1(String in) {
    System.out.println("Message from queue1: " + in);
  }

  @RabbitListener(queues = "#{anonymousQueue2.name}")
  public void receiver2(String in) {
    System.out.println("Message from queue2: " + in);
  }
}
