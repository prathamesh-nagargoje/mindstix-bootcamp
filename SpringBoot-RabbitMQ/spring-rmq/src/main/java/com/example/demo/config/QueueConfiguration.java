package com.example.demo.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfiguration {

  @Bean
  public Queue myQueue() {
    return new Queue("myQueue", false);
  }

  @RabbitListener(queues = "myQueue")
  public void listen(String in) {
    System.out.println("Message from queue: " + in);
  }
}
