package com.example.demo.service;

import java.util.List;
import java.util.Optional;
import com.example.demo.model.Customer;

public interface CustomerService {

  Optional<Customer> findById(int id);

  List<Customer> findAll();

  boolean save(Customer customer);
}
