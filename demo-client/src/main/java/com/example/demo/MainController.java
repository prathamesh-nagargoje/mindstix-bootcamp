package com.example.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class MainController {

  private static boolean cc = true;

  @GetMapping
  public ResponseEntity<Object> getCustomer() {
    if (cc) {
      cc = false;
      return ResponseEntity.ok("Customer:{}");
    }
    cc = true;
    return ResponseEntity.badRequest().body("error");
  }
}
