package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.repo.CustomerRepo;

@RestController
@RequestMapping("/customer")
public class MainController {

  @Autowired
  private CustomerRepo customerRepo;

  @GetMapping
  public String getCustomer() {
    return customerRepo.getCustomer();
  }
}
