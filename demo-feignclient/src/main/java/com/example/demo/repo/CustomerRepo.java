package com.example.demo.repo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "spring-cloud-eureka-client")
public interface CustomerRepo {

  @GetMapping("/customer")
  String getCustomer();

}
